/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	networkv1alpha1 "github.com/example/cloudinfra-operator/api/v1alpha1"
	porchdev "github.com/example/cloudinfra-operator/internal/pkg"
	appsv1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"reflect"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	ctrllog "sigs.k8s.io/controller-runtime/pkg/log"
)

// XAwsAzureNetworkReconciler reconciles a XAwsAzureNetwork object
type XAwsAzureNetworkReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=network.example.com,resources=xawsazurenetworks,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=network.example.com,resources=xawsazurenetworks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=network.example.com,resources=xawsazurenetworks/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the XAwsAzureNetwork object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *XAwsAzureNetworkReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := ctrllog.FromContext(ctx)

	log.Info("Recocile logic starts ------")

	// Fetch the instance
	xnetwork := &networkv1alpha1.XAwsAzureNetwork{}
	log.Info(xnetwork.Name)
	err := r.Get(ctx, req.NamespacedName, xnetwork)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.Info("XAwsAzureNetwork resource not found. Ignoring since object must be deleted")

			if err := porchdev.DeletePackageVariant(req.Name, "default"); err != nil {
				return ctrl.Result{}, err
			}
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "Failed to get XAwsAzureNetwork")
		return ctrl.Result{}, err
	}

	// Deletion logic - TODO need to revisit
	finalizerName := "policy.aarna.com/finalizer"
	if !xnetwork.GetObjectMeta().GetDeletionTimestamp().IsZero() {
		log.Info("Delete Information for network", "Name", xnetwork.Name)
		if err := porchdev.DeletePackageVariant(req.Name, "default"); err != nil {
			return ctrl.Result{}, err
		}

		if controllerutil.ContainsFinalizer(xnetwork, finalizerName) {
			controllerutil.RemoveFinalizer(xnetwork, finalizerName)
			log.Info("Update after removing finalizer")
			if err := r.Update(ctx, xnetwork); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, nil // Return early after handling deletion
	}

	log.Info("Going to call packagevarintset")
	err = porchdev.CreatePackageVariant(xnetwork.Name, xnetwork.Namespace, xnetwork.Spec)
	if err != nil {
		log.Error(err, "Not able to create packagevariant")

	} else {
		log.Info("package variant created successfully")
	}
	log.Info("Finished call packagevarintset")
	networkName := xnetwork.ObjectMeta.Name
	// Update status.Nodes if needed
	if !reflect.DeepEqual(networkName, xnetwork.Status.NetworkName) {
		xnetwork.Status.NetworkName = networkName
		err := r.Status().Update(ctx, xnetwork)
		if err != nil {
			log.Error(err, "Failed to update status")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *XAwsAzureNetworkReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&networkv1alpha1.XAwsAzureNetwork{}).
		//Owns(&appsv1.Deployment{}).
		Complete(r)
}

func (r *XAwsAzureNetworkReconciler) deploymentForxnetwork(xnetwork *networkv1alpha1.XAwsAzureNetwork) *appsv1.Deployment {

	return nil
}
