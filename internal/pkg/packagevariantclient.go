package porchdev

import (
	"context"
	"fmt"
	"github.com/example/cloudinfra-operator/api/v1alpha1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

func CreatePackageVariant(name string, namespace string, spec v1alpha1.XAwsAzureNetworkSpec) error {
	c, err := client.New(config.GetConfigOrDie(), client.Options{})
	if err != nil {
		fmt.Println(err)
		return err
	}

	connectivity := spec.Connectivity
	if connectivity != "vpn" {
		fmt.Println("Only connectivity VPN is supported")
		return nil
	}
	var awsvpc v1alpha1.VPC
	var azurevpc v1alpha1.VPC
	vpcs := spec.Resources.Vpcs
	for _, vpc := range vpcs {
		if vpc.Cloud == "aws" {
			awsvpc = vpc
		} else if vpc.Cloud == "azure" {
			azurevpc = vpc
		}
	}

	u := &unstructured.Unstructured{}
	u.Object = map[string]interface{}{
		"metadata": map[string]interface{}{
			"name":      name + "-multicloud",
			"namespace": "default",
		},
		"spec": map[string]interface{}{
			"annotations": map[string]interface{}{
				"approval.nephio.org/policy": "initial",
			},
			"upstream": map[string]interface{}{
				"package":  spec.KptInfo.Upstream.Package,
				"repo":     spec.KptInfo.Upstream.Repo,
				"revision": spec.KptInfo.Upstream.Revision,
			},
			"downstream": map[string]interface{}{
				"package": spec.KptInfo.Downstream.Package,
				"repo":    spec.KptInfo.Downstream.Repo,
			},
			"pipeline": map[string]interface{}{
				"mutators": []map[string]interface{}{},
			},
		},
	}
	u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
		appendMutator(u, getReplaceMutator("spec.**.azureregion", azurevpc.Region))
	u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
		appendMutator(u, getReplaceMutator("spec.**.awsregion", awsvpc.Region))

	if awsvpc.IpRange != "" {
		u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
			appendMutator(u, getReplaceMutator("spec.**.awsvpccidrblock", awsvpc.IpRange))
	}
	for _, subnet := range awsvpc.Subnets {
		if subnet.Type == "public" && subnet.Range != "" {
			u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
				appendMutator(u, getReplaceMutator("spec.**.awssubnetcidrblock", subnet.Range))
		}
	}

	if azurevpc.IpRange != "" {
		u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
			appendMutator(u, getReplaceMutator("spec.**.azurevnetaddress", azurevpc.IpRange))
	}
	for _, subnet := range azurevpc.Subnets {
		if subnet.Type == "public" && subnet.Range != "" {
			u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"] =
				appendMutator(u, getReplaceMutator("spec.**.azuresubnetaddress", subnet.Range))
		}
	}

	u.SetGroupVersionKind(getPackageVariantGVK())
	fmt.Println(u)
	return c.Create(context.Background(), u)
}

func appendMutator(u *unstructured.Unstructured, mutator map[string]interface{}) []map[string]interface{} {
	return append(
		u.Object["spec"].(map[string]interface{})["pipeline"].(map[string]interface{})["mutators"].([]map[string]interface{}),
		mutator,
	)
}

func getReplaceMutator(path, value string) map[string]interface{} {
	return map[string]interface{}{
		"configMap": map[string]interface{}{
			"by-path":   path,
			"put-value": value,
		},
		"image": "gcr.io/kpt-fn/search-replace:v0.2",
	}
}

func getPackageVariantGVK() schema.GroupVersionKind {
	return schema.GroupVersionKind{
		Group:   "config.porch.kpt.dev",
		Kind:    "PackageVariant",
		Version: "v1alpha1",
	}
}

func DeletePackageVariant(name, namespace string) error {
	// Initialize the Kubernetes client.
	c, err := client.New(config.GetConfigOrDie(), client.Options{})
	if err != nil {
		fmt.Println(err)
		return err
	}

	// Create an Unstructured object for PackageVariant.
	u := &unstructured.Unstructured{}
	u.SetGroupVersionKind(getPackageVariantGVK())
	u.SetNamespace(namespace)
	u.SetName(name + "-multicloud")

	// Delete the PackageVariant resource.
	err = c.Delete(context.Background(), u)
	if err != nil {
		fmt.Println(err)
		return err
	}

	fmt.Printf("Deleted PackageVariant %s in namespace %s\n", name+"core", namespace)
	return nil
}
