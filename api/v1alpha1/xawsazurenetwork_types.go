/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type awsresources struct {
	Region     string `json:"region"`
	VpcCIDR    string `json:"vpcCIDR,omitempty"`
	SubnetCIDR string `json:"subnetCIDR,omitempty"`
}

type azureresources struct {
	Region          string `json:"region"`
	VnetAddress     string `json:"vnetAddress,omitempty"`
	SubnetAddress   string `json:"subnetAddress,omitempty"`
	SubnetGWAddress string `json:"subnetGWAddress,omitempty"`
}

type VPC struct {
	Cloud   string   `json:"cloud"`
	Region  string   `json:"region"`
	IpRange string   `json:"iprange"`
	Subnets []subnet `json:"subnets,omitempty"`
}

type subnet struct {
	Type  string `json:"type"`
	Range string `json:"range"`
}

type Resources struct {
	Vpcs []VPC `json:"vpcs"`
}

// XAwsAzureNetworkSpec defines the desired state of XAwsAzureNetwork
type XAwsAzureNetworkSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of XAwsAzureNetwork. Edit xawsazurenetwork_types.go to remove/update
	// Foo string `json:"foo,omitempty"`

	// awsresources related to aws resources information
	//AwsResources *awsresources `json:"awsResources"`

	// azureresources related to azure resources information
	//AzureResources *azureresources `json:"azureResources"`

	Connectivity string     `json:"connectivity"`
	Resources    *Resources `json:"resources"`

	// KPTInfo holds the package name, upstream and downstream
	KptInfo *KptInfo `json:"kptInfo,omitempty"`
}

type KptInfo struct {
	DeploymentType string     `json:"deploymentType"`
	Upstream       UpStream   `json:"upstream"`
	Downstream     DownStream `json:"downstream"`
}

type UpStream struct {
	Repo     string `json:"repo"`
	Package  string `json:"package"`
	Revision string `json:"revision"`
}

type DownStream struct {
	Repo    string `json:"repo"`
	Package string `json:"package"`
}

// XAwsAzureNetworkStatus defines the observed state of XAwsAzureNetwork
type XAwsAzureNetworkStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// NetworkName is the name of the network
	NetworkName string `json:"networkname"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// XAwsAzureNetwork is the Schema for the xawsazurenetworks API
type XAwsAzureNetwork struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   XAwsAzureNetworkSpec   `json:"spec,omitempty"`
	Status XAwsAzureNetworkStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// XAwsAzureNetworkList contains a list of XAwsAzureNetwork
type XAwsAzureNetworkList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []XAwsAzureNetwork `json:"items"`
}

func init() {
	SchemeBuilder.Register(&XAwsAzureNetwork{}, &XAwsAzureNetworkList{})
}
